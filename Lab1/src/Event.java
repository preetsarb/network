public class Event implements Comparable<Event> {

    double time;
    TYPE evType;
    Source src;
    Switch sw;

    Event(double time, TYPE evType, Source src, Switch sw) {
        this.time = time;
        this.evType = evType;
        this.src = src;
        this.sw = sw;
    }

    public void performAction() {
        switch (evType) {
            case PACKET_GENERATION:
                generatePacket();
                break;
            case TRANSMISSION_TO_SWITCH:
                transmitPacket();
                break;
            case TRANSMISSION_TO_SINK:
                removePacket();
                break;
            default:
        }
    }

    public void generatePacket() {
        double nextTime = (time + (1 / src.psr));
        Event e = new Event(nextTime, TYPE.PACKET_GENERATION, src, sw);
        Network.ev.add(e);
        if (src.packetCount < Network.queue_size) {
            Packet p = new Packet(src.id, nextTime);
            src.buffer.add(p);
            src.packetCount++;
        } else {
            src.packetsDroppedatSource += 1;
        }
        time += 0.000000001;

        //if link is empty transmit the packet immediately
        if (src.flag1) {
            e = new Event((time + (Network.packet_size / src.linkBW)), TYPE.TRANSMISSION_TO_SWITCH, src, sw);
            Network.ev.add(e);
            src.flag1 = false;
        }
    }

    public void transmitPacket() {
        if (src.packetCount > 0) {
            Network.pacRec += 1;
            double nextTime = (time + (Network.packet_size / src.linkBW));
            Packet p = src.buffer.poll();
            src.packetCount--;
            if (sw.tdm) {
                if (sw.sQueues[src.id - 1].size() < Network.sw_queue_size) {
                    sw.sQueues[src.id - 1].add(p);
                    sw.packetCount++;
                } else {
                    src.packetsDroppedatSwitch += 1;
                    sw.packetsDropped += 1;
                }
            } else {
                if (sw.packetCount < Network.sw_queue_size) {
                    sw.buffer.add(p);
                    sw.packetCount++;
                } else {
                    src.packetsDroppedatSwitch += 1;
                    sw.packetsDropped += 1;
                }
            }
            Event e = new Event(nextTime, TYPE.TRANSMISSION_TO_SWITCH, src, sw);
            Network.ev.add(e);
            if (sw.flag) {
                if (sw.tdm) {
                    e = new Event((time + ((sw.nSources * Network.packet_size) / sw.linkBW)), TYPE.TRANSMISSION_TO_SINK, src, sw);
                } else {
                    e = new Event((time + (Network.packet_size / sw.linkBW)), TYPE.TRANSMISSION_TO_SINK, src, sw);
                }
                Network.ev.add(e);
                sw.flag = false;
            }
        } else {
            src.flag1 = true;
        }
    }

    public void removePacket() {
        if (sw.packetCount > 0) {
            double nextTime;
            if (sw.tdm) {
                //length of time slot = time taken to transmit packets from all sources
                nextTime = (time + ((sw.nSources * Network.packet_size) / sw.linkBW));
                for (int i = 0; i < sw.nSources; i++) {
                    if (sw.sQueues[i].size() > 0) {
                        sw.sQueues[i].poll();
                        sw.packetCount--;
                    }
                }
            } else {
                nextTime = (time + ((Network.packet_size) / sw.linkBW));
                sw.buffer.poll();
                sw.packetCount--;
            }
            Event e = new Event(nextTime, TYPE.TRANSMISSION_TO_SINK, src, sw);
            Network.ev.add(e);
        } else {
            sw.flag = true;
        }
    }


    @Override
    public int compareTo(Event o) {
        if (this.time > o.time) {
            return 1;
        } else if (this.time < o.time) {
            return -1;
        } else {
            return 0;
        }
    }

    public enum TYPE {
        PACKET_GENERATION, TRANSMISSION_TO_SWITCH, TRANSMISSION_TO_SINK
    }
}