import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

public class Network {
    public final static int packet_size = 100;            // 100 Kb
    public static int queue_size = 100;            // source queue size
    public static int sw_queue_size = 100;           // switch queue size
    public static Queue<Event> ev;
    public static float pacRec = 0;

    public void simulate(int itr) throws IOException {
        String fn = "in" + itr + ".txt";
        Scanner inp = new Scanner(new FileReader(fn));
        int nSources;
        System.out.println("Enter no. of sources: ");
        nSources = inp.nextInt();
        Source[] src = new Source[nSources];

        System.out.println("Same or different type of sources: \n1. Same\n2. Different\n");
        switch (inp.nextInt()) {
            case 1:
                System.out.println("Packet Sending Rate of sources (P/sec): ");
                float pr = inp.nextFloat();
                System.out.println("Link BandWidth of sources (Kb/sec): ");
                float bw = inp.nextFloat();
                for (int i = 0; i < nSources; i++) {
                    src[i] = new Source(i + 1, pr, bw);
                }
                break;
            case 2:
                for (int i = 0; i < nSources; i++) {
                    System.out.println("Packet Sending Rate of source " + (i + 1) + " (P/sec): ");
                    pr = inp.nextFloat();
                    System.out.println("Link BandWidth of source " + (i + 1) + " (Kb/sec): ");
                    bw = inp.nextFloat();
                    src[i] = new Source(i + 1, pr, bw);
                }
                break;
            default:
                System.out.println("Wrong choice!");
                System.exit(0);
        }

        System.out.println("Enter band width of Switch (Kb/sec): ");
        float temp = inp.nextFloat();
        boolean tdm = false;

        System.out.println("Type of switch:\n1. Packet Switching\n2. TDM");
        switch (inp.nextInt()) {
            case 1:
                tdm = false;
                break;
            case 2:
                tdm = true;
                break;
            default:
                System.out.println("Wrong Choice!");
                System.exit(0);
        }
        Switch sw = new Switch(temp, tdm, nSources);

        ev = new PriorityQueue<Event>();                    //event queue
        Random r = new Random();

        //adding first generation event for each source
        for (int i = 0; i < nSources; i++) {
            Event tmp = new Event(((1 / src[i].psr) + r.nextDouble()), Event.TYPE.PACKET_GENERATION, src[i], sw);
            ev.add(tmp);
        }

        fn = "C:\\Users\\Sarbpreet\\IdeaProjects\\NetworksLab\\networkslab\\Lab1\\output\\itr" + itr + "\\";

        BufferedWriter[] bw = new BufferedWriter[nSources];          //files for individual source data
        BufferedWriter bw1 = new BufferedWriter(new FileWriter(fn + "out" + itr + ".txt"));       //file for switch data
        bw1.write("SW Q\tPD SW\tThrPt\t\tTime\t\tType\n");   //Switch queue size  +   packets dropped  +  time + throughput +  type of event
        for (int i = 0; i < nSources; i++) {
            bw[i] = new BufferedWriter(new FileWriter(fn + "src" + (i + 1) + ".txt"));
            bw[i].write("Src" + (i + 1) + " Q\tSW Q\tPD Src\tPD SW\tTime\t\tType");   //Source queue size  +  switch queue size  +  packets dropped  +  time  +  type of event
            bw[i].newLine();
        }


        //processing events one by one
        int N = 10000;
        while (N > 0) {
            Event e = ev.poll();
            int sid = e.src.id - 1;
            e.performAction();
            if (N % 10 == 0) {
                if (sw.packetCount != 0) {
                    bw1.write(sw.packetCount + "\t" + sw.packetsDropped + "\t " + (pacRec - sw.packetsDropped) / pacRec + "\t\t" + e.time + "\t\t" + e.evType);
                }
                bw[sid].write(e.src.buffer.size() + "\t" + sw.packetCount + "\t" + e.src.packetsDroppedatSource + "\t" + e.src.packetsDroppedatSwitch + "\t" + e.time + "\t\t" + e.evType);
                bw[sid].newLine();
                bw1.newLine();
            }
            N--;
        }
        for (int i = 0; i < nSources; i++) {
            bw[i].close();
        }
        bw1.close();
    }
}

class Switch {
    float linkBW;
    Queue<Packet> buffer;      //for packet switching
    int packetCount;
    int packetsDropped;
    boolean tdm;
    Queue[] sQueues;   //for TDM
    int nSources;
    boolean flag;

    Switch(float linkBW, boolean tdm, int nSources) {
        this.linkBW = linkBW;
        packetCount = 0;
        packetsDropped = 0;
        this.tdm = tdm;
        this.nSources = nSources;
        flag = true;
        if (this.tdm) {
            sQueues = new Queue[nSources];
            for (int i = 0; i < nSources; i++) {
                sQueues[i] = new LinkedList<Packet>();
            }
        } else {
            buffer = new PriorityQueue<Packet>();
        }
    }
}

class Source {
    int id;     //Source id
    float psr;    // Packet Sending Rate
    float linkBW;             //Link Bandwidth
    Queue<Packet> buffer;       //buffer at source
    int packetCount;            //count for monitoring buffer overflow
    boolean flag1;
    int packetsDroppedatSource;
    int packetsDroppedatSwitch;

    Source(int id, float psr, float linkBW) {
        this.id = id;
        this.psr = psr;
        this.linkBW = linkBW;
        packetCount = 0;
        buffer = new PriorityQueue<Packet>();
        flag1 = true;
        packetsDroppedatSource = 0;
        packetsDroppedatSwitch = 0;
    }
}

class Packet implements Comparable<Packet> {
    int sourceID;
    double timestamp;

    Packet(int sId, double ts) {
        sourceID = sId;
        timestamp = ts;
    }

    @Override
    public int compareTo(Packet o) {
        if (this.timestamp > o.timestamp) {
            return 1;
        } else if (this.timestamp < o.timestamp) {
            return -1;
        } else {
            return 0;
        }
    }
}
