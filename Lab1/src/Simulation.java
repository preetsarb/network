import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class Simulation {
    public static void main(String[] args) throws IOException {
        int n = 5;
        int same = 1;
        int diff = 2;
        float[][] srcBw = { {100, 500, 500, 500, 500},               //link band width of sources for different iterations
                            {200, 200, 200, 200, 200},
                            {300, 500, 500, 500, 500},
                            {500, 1000, 1000, 1000, 200}};
        float[][] srcPGR = {{4, 3, 5, 4, 2},                         //packet generation rate of sources for different iterations
                            {3, 6, 2, 3, 9},
                            {5, 7, 4, 3, 5},
                            {8, 7, 4, 3, 3}};

        float swBW[] = {800,500,800,1500};                           //link band width of switch for different iterations
        int ps = 1;
        int tdm = 2;

        for (int i = 1; i < 5; i++) {
            BufferedWriter bw = new BufferedWriter(new FileWriter("in" + i + ".txt"));
            bw.write(n + "\n" + same);
            bw.newLine();
            bw.write(srcPGR[0][0] + "\t" + srcBw[i-1][0]);
            bw.newLine();

            bw.write(swBW[0] + "\n" + tdm);
            bw.newLine();
            bw.close();
            Network net = new Network();
            net.simulate(i);
        }


    }
}
