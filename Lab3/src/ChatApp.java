import javax.swing.*;
import javax.swing.text.BadLocationException;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;

public class ChatApp {

    private JFrame mainFrame;
    private JPanel panel1;
    private JTextPane chatText;
    private JTextField remoteIpField;
    private JTextField remotePortField;
    private JButton connectButton;
    private JTextField chatLine;
    private JButton sendButton;
    private JButton serverSettingsButton;
    private JButton exitButton;
    private JPanel statusIndicator;
    private JLabel statusText;
    private JLabel localAddress;
    private JScrollPane scrPane;
    private Style style;
    private StyledDocument doc;

    private volatile boolean connected;
    private volatile ServerSocket welcomeSocket;
    private volatile Socket socket;
    private volatile BufferedReader streamIn;
    private volatile PrintWriter streamOut;
    private volatile StringBuffer toSend;
    private String localIP;
    private int localPORT = 7777;
    private String remoteIP;

    public static void main(String[] args) {
        ChatApp c = new ChatApp();
        c.initialize();
    }

    private void initialize() {
        initGUI();
        startChat();
    }

    private void startChat() {

        JScrollBar vertical = scrPane.getVerticalScrollBar();
        boolean serverStarted = false;
        while (!serverStarted) {
            try {
                welcomeSocket = new ServerSocket(localPORT);
                serverStarted = true;
                localAddress.setText("  " + localIP + " : " + localPORT + "  ");
            } catch (IOException e) {
                JOptionPane.showMessageDialog(null,"Server can't start at port "+localPORT+".\nPlease change port number in Server Settings.");
                String s = (String) JOptionPane.showInputDialog(
                        mainFrame,
                        "Server Settings",
                        "Enter Port on which you want Server to run: ",
                        JOptionPane.QUESTION_MESSAGE,
                        null,
                        null,
                        localPORT);
                localPORT = Integer.parseInt(s);
                //e.printStackTrace();
            }
        }

        try {
            //System.out.println("Waiting");
            //System.out.println("Server at " + localPORT);
            socket = welcomeSocket.accept();
            remoteIP = socket.getInetAddress().getHostAddress();
            updateGUI(true);
        } catch (IOException e) {
            //e.printStackTrace();
        }

        try {
            streamIn = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            streamOut = new PrintWriter(socket.getOutputStream());
            toSend = new StringBuffer();
        } catch (IOException e) {
           // e.printStackTrace();
        }catch (NullPointerException ex){
            //ex.printStackTrace();
        }

        chatText.setText("");
        streamOut.println("Connection Successful!");
        streamOut.flush();

        //System.out.println("Started");
        while (connected) {
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            try {
                if (streamIn.ready()) {
                    // System.out.println("INPUT");
                    String in = streamIn.readLine();
                    if (in.equals(":exit")) {
                        dissconnect();
                        break;
                    }
                    StyleConstants.setForeground(style, Color.BLUE);
                    doc.insertString(doc.getLength(), "IN: " + in + "\n", style);
                    vertical.setValue(vertical.getMaximum());
                }
            } catch (IOException e) {
                e.printStackTrace();
            } catch (BadLocationException e) {
                e.printStackTrace();
            }

            if (toSend.length() > 0) {
                String out = toSend.toString();
                StyleConstants.setForeground(style, Color.RED);
                try {
                    doc.insertString(doc.getLength(), "YOU: " + out + "\n", style);
                    vertical.setValue(vertical.getMaximum());
                    streamOut.println(out);
                    streamOut.flush();
                    toSend.setLength(0);
                } catch (BadLocationException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void connect() {
        remoteIP = remoteIpField.getText();
        int remotePORT = Integer.parseInt(remotePortField.getText());
        try {
            socket = new Socket(remoteIP, remotePORT);
            updateGUI(true);
            welcomeSocket.close();
        } catch (IOException e) {
            JOptionPane.showMessageDialog(null,"Can't connect to remote server.\nPlease check IP address and PORT.");
           // e.printStackTrace();
        }
    }

    private void dissconnect() {
        //System.out.println("Dis");
        if(streamOut!=null) {
            streamOut.println(":exit");
            streamOut.flush();
        }
        cleanUP();
        updateGUI(false);
        new Thread(new Runnable() {
            @Override
            public void run() {
                startChat();
            }
        }).start();
        //System.out.println("cl2");
    }

    private void updateGUI(boolean state) {
        connected = state;
        remoteIpField.setEnabled(!state);
        remotePortField.setEnabled(!state);
        if (state) {
            connectButton.setText("Disconnect");
            statusIndicator.setBackground(Color.GREEN);
            statusText.setText("Connected to - " + remoteIP + "  ");
        } else {
            connectButton.setText("Connect");
            statusIndicator.setBackground(Color.RED);
            statusText.setText("Disconnected");
        }
        chatLine.setEnabled(state);
        sendButton.setEnabled(state);
    }

    private void send() {
        String in = chatLine.getText();
        if (!in.equals("")) {
            toSend.append(in);
            chatLine.setText("");
        }
    }

    private void cleanUP() {
        connected = false;
        //System.out.println("cl");
        try {
            if (welcomeSocket != null && !welcomeSocket.isClosed()) {
                welcomeSocket.close();
            }
            if (socket != null && !socket.isClosed()) {
                socket.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void serverSettings() {
        String s = (String) JOptionPane.showInputDialog(
                mainFrame,
                "Server Settings",
                "Enter Port on which you want Server to run: ",
                JOptionPane.QUESTION_MESSAGE,
                null,
                null,
                localPORT);
        if(!s.trim().equals("")) {
            localPORT = Integer.parseInt(s);
            localAddress.setText("  " + localIP + " : " + localPORT + "  ");
            dissconnect();
        }
    }

    private void initGUI() {
        Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
            @Override
            public void run() {
                if(streamOut!=null) {
                    streamOut.println(":exit");
                    streamOut.flush();
                }
                cleanUP();
            }
        }));

        try {
            localIP = InetAddress.getLocalHost().getHostAddress();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }

        //System.out.println(localIP);

        statusIndicator.setBackground(Color.RED);
        statusText.setText("Disconnected");

        doc = chatText.getStyledDocument();
        style = doc.addStyle("Style", null);
        connectButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (e.getActionCommand().equals("Connect")) {
                    connect();
                } else {
                    dissconnect();
                }
            }
        });

        sendButton.setEnabled(false);
        chatLine.setEnabled(false);

        sendButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                send();
            }
        });

        chatLine.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                super.keyPressed(e);
                if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                    send();
                }
            }
        });

        exitButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });

        serverSettingsButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                serverSettings();
            }
        });


        mainFrame = new JFrame();
        mainFrame.setContentPane(panel1);
        mainFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        mainFrame.setSize(new Dimension(550, 500));
        mainFrame.setResizable(false);
        mainFrame.setVisible(true);
    }


}
