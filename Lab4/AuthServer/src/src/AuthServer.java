package src;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.*;
import java.util.ArrayList;
import java.util.Scanner;

public class AuthServer {
    private static int SERVER_PORT = 7777;
    private static ServerSocket welcomeSocket;
    private static volatile boolean flag = true;

    public static void main(String[] args) {

        try {

            Class.forName("org.sqlite.JDBC");
            Connection conn = DriverManager.getConnection("jdbc:sqlite:chatApp.db");
            Statement stmt = conn.createStatement();

            String sql = "CREATE TABLE IF NOT EXISTS USERS " +
                    "(USERNAME    CHAR(50)  PRIMARY KEY    NOT NULL, " +
                    " PASSWORD    CHAR(50)                 NOT NULL, " +
                    " IP          CHAR(50)   DEFAULT ''," +
                    " PORT        INT        DEFAULT 7777," +
                    " STATUS      INT        DEFAULT 0)";
            stmt.executeUpdate(sql);

            sql = "CREATE TABLE IF NOT EXISTS FRIENDS " +
                    "(USER1     CHAR(50)  NOT NULL," +
                    " USER2     CHAR(50)  NOT NULL)";
            stmt.executeUpdate(sql);

            sql = "CREATE TABLE IF NOT EXISTS GROUPS " +
                    "(NAME          CHAR(50)  PRIMARY KEY   NOT NULL," +
                    " INITIATOR     CHAR(50)  NOT NULL," +
                    " IP            CHAR(50)  NOT NULL," +
                    " PORT          INT       DEFAULT  6789)";

            stmt.executeUpdate(sql);
            stmt.close();
            conn.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
            @Override
            public void run() {
                flag = false;
                try {
                    welcomeSocket.close();
                    welcomeSocket = null;
                } catch (IOException e) {
                    welcomeSocket = null;
                    e.printStackTrace();
                }
            }
        }));

        boolean started = false;
        while (!started) {
            try {
                welcomeSocket = new ServerSocket(SERVER_PORT);
                System.out.println("Server started at port " + SERVER_PORT);
                started = true;
            } catch (IOException e) {
                //e.printStackTrace();
                System.out.println("Server can't start at port " + SERVER_PORT);
                System.out.println("Please enter new port (5000-65535): ");
                Scanner inp = new Scanner(System.in);
                try {
                    SERVER_PORT = inp.nextInt();
                } catch (Exception ignored) {

                }
            }
        }

        while (flag) {
            try {
                Socket clientSocket = welcomeSocket.accept();
                if (clientSocket != null) {
                    new Thread(new ConnectionThread(clientSocket)).start();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}

class ConnectionThread implements Runnable {

    private Connection conn;
    private BufferedReader streamIn;
    private PrintWriter streamOut;
    private String clientIPAddress;
    private int clientPORT;
    private String username;
    private Socket clientSocket;
    private boolean isLoggedIn;

    ConnectionThread(Socket clientSocket) {

        isLoggedIn = false;
        try {
            conn = DriverManager.getConnection("jdbc:sqlite:chatApp.db");
            this.clientSocket = clientSocket;
            streamIn = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            streamOut = new PrintWriter(clientSocket.getOutputStream(), true);
            clientIPAddress = clientSocket.getInetAddress().getHostAddress();
        } catch (Exception e) {
            cleanUp();
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        boolean running = true;
        System.out.println("Client accepted " + clientIPAddress);
        int time = 0;
        int tries = 0;
        while (running) {
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            //heartbeat for checking if client is still connected
            time += 100;
            if (time > 5 * 1000) {
                time = 0;
                try {
                    byte[] b = {':', 'b', 'e', 'a', 't', '\n'};
                    clientSocket.getOutputStream().write(b);
                    clientSocket.getOutputStream().flush();
                    tries = 0;
                } catch (IOException e) {
                    tries++;
                    if (tries >= 2) {
                        System.out.println("Closed!");
                        logout();
                        running = false;
                    }
                }
            }

            try {
                int code;
                try {
                    if (streamIn.ready()) {
                        code = Integer.parseInt(streamIn.readLine());
                    } else {
                        continue;
                    }
                } catch (NumberFormatException e) {
                    // e.printStackTrace();
                    code = 0;
                }
                System.out.println(code);
                //client sends different codes for different activities
                switch (code) {
                    case 100:   //signup
                        streamOut.println(signUP());
                        streamOut.flush();
                        running = false;
                        break;
                    case 200:    //login
                        int res = login();
                        streamOut.println(res);
                        streamOut.flush();
                        if (res != 0) {
                            running = false;
                        }
                        break;
                    case 300:   //logout
                        streamOut.println(logout());
                        streamOut.flush();
                        running = false;
                        break;
                    case 400:   //add friend
                        streamOut.println(addFriend());
                        streamOut.flush();
                        break;
                    case 500:   //get friend list
                        friendList();
                        break;
                    case 600:   //update port if changed
                        streamOut.println(updatePort());
                        streamOut.flush();
                        break;
                    case 700:   //get your network address
                        streamOut.println(clientIPAddress);
                        streamOut.flush();
                        break;
                    case 800:   //update status
                        streamOut.println(updateStatus());
                        streamOut.flush();
                        break;
                    case 888:   //create chat group
                        streamOut.println(createGroup());
                        streamOut.flush();
                        break;
                    case 900:   //get group list
                        groupList();
                        break;
                    case 999:   //close group
                        streamOut.println(removeGroup());
                        streamOut.flush();
                        break;
                    default:
                }
            } catch (Exception ioe) {
                ioe.printStackTrace();
                running = false;
            }
        }
        cleanUp();
    }

    private void cleanUp() {
        if (conn != null) {
            try {
                conn.close();
                conn = null;
            } catch (SQLException e) {
                conn = null;
            }
        }

        if (clientSocket != null) {
            try {
                clientSocket.close();
                clientSocket = null;
            } catch (IOException e) {
                clientSocket = null;
            }
        }

        if (streamIn != null) {
            try {
                streamIn.close();
                streamIn = null;
            } catch (IOException e) {
                streamIn = null;
            }
        }

        if (streamOut != null) {
            streamOut.close();
            streamOut = null;
        }
    }

    private int signUP() {
        try {
            username = streamIn.readLine().trim();
            String password = streamIn.readLine().trim();
            Statement stmt = conn.createStatement();
            String sql = "SELECT COUNT(*) as rowCount FROM USERS where USERNAME = '" + username + "';";
            ResultSet rs = stmt.executeQuery(sql);

            if (rs.getInt("rowCount") > 0) {
                stmt.close();
                rs.close();
                return 1;    //user already exist
            } else {
                sql = "INSERT INTO USERS (USERNAME,PASSWORD) VALUES ('" + username + "', '" + password + "');";
                stmt.executeUpdate(sql);
                //conn.commit();
                stmt.close();
                rs.close();
                return 0;    //success
            }
        } catch (Exception e) {
            e.printStackTrace();
            return -1;      //error in database
        }
    }

    private int login() {
        try {
            username = streamIn.readLine().trim();
            String password = streamIn.readLine().trim();
            clientPORT = Integer.parseInt(streamIn.readLine().trim());
            Statement stmt = conn.createStatement();
            String sql = "SELECT COUNT(*) as rowCount FROM USERS WHERE USERNAME = '" + username + "' AND PASSWORD = '" + password + "';";
            ResultSet rs = stmt.executeQuery(sql);
            if (rs.getInt("rowCount") == 1) {
                rs.close();
                stmt.close();
                try {
                    updateIP(0);
                    System.out.println("Done");
                    isLoggedIn = true;
                    return 0;    // success
                } catch (SQLException e) {
                    e.printStackTrace();
                    return -1;   //error in database
                }
            } else {
                rs.close();
                stmt.close();
                System.out.println("Fail");
                return 1;    //wrong username or password
            }
        } catch (Exception e) {
            e.printStackTrace();
            return -1;   //error in database
        }
    }

    private int logout() {
        try {
            updateIP(1);
            isLoggedIn = false;
            return 0;
        } catch (SQLException e) {
            e.printStackTrace();
            return -1;
        }

    }

    private int addFriend() {
        if (isLoggedIn) {
            try {
                String user2 = streamIn.readLine();
                if (username.equals(user2)) {
                    return 3;   //can't add yourself
                }
                Statement stmt = conn.createStatement();
                ResultSet rs = stmt.executeQuery("SELECT COUNT(*) as rowCount FROM USERS WHERE USERNAME = '" + user2 + "';");
                if (rs.getInt("rowCount") > 0) {
                    rs.close();
                    rs = stmt.executeQuery("SELECT COUNT(*) as rowCount FROM FRIENDS WHERE USER1 = '" + username + "' AND USER2 = '" + user2 + "';");
                    if (rs.getInt("rowCount") > 0) {
                        rs.close();
                        stmt.close();
                        return 2;     //already a friend
                    } else {
                        stmt.executeUpdate("INSERT INTO FRIENDS (USER1,USER2) VALUES ('" + username + "', '" + user2 + "');");
                        //conn.commit();
                        stmt.executeUpdate("INSERT INTO FRIENDS (USER1,USER2) VALUES ('" + user2 + "', '" + username + "');");
                        //conn.commit();
                        rs.close();
                        stmt.close();
                        return 0;         //success
                    }
                } else {
                    rs.close();
                    stmt.close();
                    return 1;               //user doesn't exist
                }
            } catch (Exception e) {
                e.printStackTrace();
                return -1;                  //error in database
            }

        } else {
            return 3;     //not logged in
        }
    }

    private void friendList() {
        if (isLoggedIn) {
            try {
                Statement stmt = conn.createStatement();
                ResultSet rs = stmt.executeQuery("SELECT * FROM FRIENDS WHERE USER1 = '" + username + "';");
                ArrayList<String> userlist = new ArrayList<String>();
                while (rs.next()) {
                    userlist.add(rs.getString("USER2"));
                }
                rs.close();
                for (String user2 : userlist) {
                    rs = stmt.executeQuery("SELECT * FROM USERS WHERE USERNAME = '" + user2 + "';");
                    int status = rs.getInt("STATUS");
                    String ip = rs.getString("IP");
                    int port = rs.getInt("PORT");
                    String toSend = user2 + "," + status + "," + ip + "," + port;
                    streamOut.println(toSend);
                    streamOut.flush();
                    rs.close();
                }
                streamOut.println(":end");
                streamOut.flush();
                rs.close();
                stmt.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void updateIP(int type) throws SQLException {
        Statement stmt;
        String sql;
        switch (type) {
            case 0:
                stmt = conn.createStatement();
                sql = "UPDATE USERS set IP = '" + clientIPAddress + "' , STATUS = '1' , PORT = " + clientPORT + "  where USERNAME='" + username + "';";
                stmt.executeUpdate(sql);
                //conn.commit();
                stmt.close();
                break;
            case 1:
                stmt = conn.createStatement();
                sql = "UPDATE USERS set STATUS = '0' where USERNAME='" + username + "';";
                stmt.executeUpdate(sql);
                //conn.commit();
                stmt.close();
                break;
            default:
        }
    }

    private int updateStatus() {
        if (isLoggedIn) {
            try {
                int status;
                try {
                    status = Integer.parseInt(streamIn.readLine());
                } catch (NumberFormatException e) {
                    return -1;
                }
                try {
                    String sql = "UPDATE USERS set  STATUS = '" + status + "'  where USERNAME='" + username + "';";
                    Statement stmt = conn.createStatement();
                    stmt.executeUpdate(sql);
                    stmt.close();
                    return 0;
                } catch (SQLException e) {
                    e.printStackTrace();
                    return -1;
                }
            } catch (IOException e) {
                e.printStackTrace();
                return -1;
            }
        } else {
            return -1;
        }
    }

    private int updatePort() {
        if (isLoggedIn) {
            try {
                try {
                    clientPORT = Integer.parseInt(streamIn.readLine());
                } catch (NumberFormatException e) {
                    return -1;
                }
                try {
                    String sql = "UPDATE USERS set  PORT = " + clientPORT + "  where USERNAME='" + username + "';";
                    Statement stmt = conn.createStatement();
                    stmt.executeUpdate(sql);
                    stmt.close();
                    return 0;
                } catch (SQLException e) {
                    e.printStackTrace();
                    return -1;
                }
            } catch (IOException e) {
                e.printStackTrace();
                return -1;
            }
        } else {
            return -1;
        }
    }

    private int createGroup() {
        if (isLoggedIn) {
            try {
                String gName = streamIn.readLine().trim();
                int port = Integer.parseInt(streamIn.readLine().trim());

                Statement stmt = conn.createStatement();
                String sql = "SELECT COUNT(*) as rowCount FROM GROUPS where NAME = '" + gName + "';";
                ResultSet rs = stmt.executeQuery(sql);

                if (rs.getInt("rowCount") > 0) {
                    stmt.close();
                    rs.close();
                    return 1;    //group already exist
                } else {
                    sql = "INSERT INTO GROUPS (NAME,INITIATOR,IP,PORT) VALUES ('" + gName + "', '" + username + "','" + clientIPAddress + "','" + port + "');";
                    stmt.executeUpdate(sql);
                    stmt.close();
                    rs.close();
                    return 0;    //success
                }

            } catch (Exception e) {
                e.printStackTrace();
                return -1;
            }
        } else {
            return -1;
        }
    }

    private void groupList() {
        if (isLoggedIn) {
            try {
                Statement stmt = conn.createStatement();
                ResultSet rs = stmt.executeQuery("SELECT * FROM GROUPS;");
                while (rs.next()) {
                    String name = rs.getString("NAME");
                    String initiator = rs.getString("INITIATOR");
                    String ip = rs.getString("IP");
                    int port = rs.getInt("PORT");
                    String toSend = name + "," + initiator + "," + ip + "," + port;
                    streamOut.println(toSend);
                    streamOut.flush();
                }
                rs.close();
                streamOut.println(":end");
                streamOut.flush();
                rs.close();
                stmt.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private int removeGroup() {
        try {
            String gName = streamIn.readLine().trim();
            Statement stmt = conn.createStatement();
            String sql = "SELECT COUNT(*) as rowCount, INITIATOR FROM GROUPS where NAME = '" + gName + "';";
            ResultSet rs = stmt.executeQuery(sql);

            if (rs.getInt("rowCount") == 1) {
                if (rs.getString("INITIATOR").equals(username)) {
                    sql = "DELETE FROM GROUPS WHERE NAME = '" + gName + "';";
                    stmt.executeUpdate(sql);
                    stmt.close();
                    rs.close();
                    return 0;    //success
                } else {
                    stmt.close();
                    rs.close();
                    return 2;    //don't have rights
                }
            } else {
                stmt.close();
                rs.close();
                return 1;    //group doesn't  exist
            }
        } catch (Exception e) {
            e.printStackTrace();
            return -1;  //db error
        }
    }

}