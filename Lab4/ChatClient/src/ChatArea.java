import javax.swing.*;

public class ChatArea extends JPanel {
    private JTextField chatLine;
    private JButton sendButton;
    private JTextPane chatText;
    private JPanel chatPanel;
    private JButton videoButton;
    private JScrollPane scrollPane;

    public JScrollPane getScrollPane() {
        return scrollPane;
    }

    public JButton getVideoButton() {
        return videoButton;
    }

    public JTextField getChatLine() {
        return chatLine;
    }

    public JButton getSendButton() {
        return sendButton;
    }

    public JTextPane getChatText() {
        return chatText;
    }

    public JPanel getChatPanel() {
        return chatPanel;
    }
}
