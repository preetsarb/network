import javax.swing.*;
import javax.swing.text.BadLocationException;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;
import java.awt.*;
import java.awt.event.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

public class ClientPane extends JFrame {
    public static volatile ArrayList<String> activeUsers;
    public static volatile ArrayList<String> activeGroups;
    public static volatile ArrayList<ChatThread> activeThreads;
    private JPanel panel1;
    private JTabbedPane chatTabs;
    private JButton serverSettingsButton;
    private JButton addFriendButton;
    private JList<User> list1;
    private JButton refreshButton;
    private JButton logOutButton;
    private JLabel userLabel;
    private JLabel localServerLabel;
    private JPanel statusColour;
    private JComboBox statusBox;
    private JList<Group> groupList;
    private JButton createButton;
    private ArrayList<User> friends;
    private ArrayList<Group> groups;
    private volatile ServerSocket welcomeSocket;
    private Socket clientSocket;
    private BufferedReader authStreamIn;
    private PrintWriter authStreamOut;
    private volatile boolean running;

    public static void main(String[] args) {
        ClientPane c = new ClientPane();
        c.initialize();
    }

    public void initialize() {
        initGUI();
        getFriendList();
        getGroupList();
        authStreamOut.println("700");
        authStreamOut.flush();
        try {
            String in = authStreamIn.readLine().trim();
            while (in.equals(":beat")) {
                in = authStreamIn.readLine().trim();
            }
            Common.localIP = in;
        } catch (IOException e) {
            e.printStackTrace();
        }
        new Thread(new Runnable() {
            @Override
            public void run() {
                startServer();
            }
        }).start();

    }

    private void initGUI() {

        Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
            @Override
            public void run() {
                authStreamOut.println("300");
                authStreamOut.flush();
                if (welcomeSocket != null && welcomeSocket.isClosed()) {
                    try {
                        welcomeSocket.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                Common.running = false;
                // System.out.println("Cl");
                synchronized (this) {
                    for (ChatThread ct : activeThreads) {
                        ct.disconnect();
                        // System.out.println("Closing ct");
                    }
                }
                //System.out.println("Closing!");
                Driver.lg.cleanUP();
            }
        }));

        userLabel.setText("  Welcome " + Common.user + "!  ");

        statusBox.setSelectedIndex(1);
        statusColour.setBackground(Color.GREEN);

        statusBox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String s = (String) statusBox.getSelectedItem();
                int status = statusBox.getSelectedIndex();
                if (s.trim().equals("Online")) {
                    statusColour.setBackground(Color.GREEN);
                } else if (s.trim().equals("Offline")) {
                    statusColour.setBackground(Color.DARK_GRAY);
                } else if (s.trim().equals("Away")) {
                    statusColour.setBackground(Color.BLUE);
                } else if (s.trim().equals("Busy")) {
                    statusColour.setBackground(Color.ORANGE);
                }
                changeStatus(status);
            }
        });

        activeUsers = new ArrayList<String>();
        activeThreads = new ArrayList<ChatThread>();
        activeGroups = new ArrayList<String>();

        groups = new ArrayList<Group>();

        serverSettingsButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                serverDetails();
            }
        });

        serverSettingsButton.setFocusPainted(false);

        logOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                logout();
            }
        });

        addFriendButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                addFriend();
            }
        });

        refreshButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                getFriendList();
                getGroupList();
            }
        });

        createButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        GroupChat gc = new GroupChat(authStreamIn, authStreamOut, true, null);
                        gc.initiate();
                    }
                }).start();
            }
        });

        list1.addMouseListener(new MouseAdapter() {

            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                if (e.getClickCount() == 1) {
                    int index = list1.getSelectedIndex();
                    getFriendList();
                    User u = friends.get(index);
                    connect(u);
                }
            }
        });

        groupList.addMouseListener(new MouseAdapter() {

            @Override
            public void mouseClicked(MouseEvent e) {
                super.mouseClicked(e);
                if (e.getClickCount() == 1) {
                    int index = groupList.getSelectedIndex();
                    Group g = groups.get(index);
                    joinGroup(g);
                }
            }
        });

        this.setContentPane(panel1);
        this.setResizable(false);
        this.setSize(new Dimension(700, 500));
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.setVisible(true);
    }

    private void startServer() {
        try {
            welcomeSocket = new ServerSocket(Common.localPORT);
            running = true;
            //System.out.println("Server started!");
            localServerLabel.setText(Common.localIP + " : " + Common.localPORT);
        } catch (IOException e) {
            //e.printStackTrace();
            JOptionPane.showMessageDialog(this, "Can't start server at port " + Common.localPORT + ".\nPlease change the local server port!", "Error", JOptionPane.ERROR_MESSAGE);
            serverDetails();
        }

        while (running) {
            try {
                clientSocket = welcomeSocket.accept();
                if (clientSocket != null) {
                    ChatThread ct = new ChatThread(clientSocket, chatTabs);
                    new Thread(ct).start();
                    activeThreads.add(ct);
                    activeUsers.add(ct.getUsername());
                }
            } catch (IOException e) {
                //e.printStackTrace();
            }
        }

    }

    private void changeStatus(int status) {
        authStreamOut.println("800");
        authStreamOut.flush();
        authStreamOut.println(status);
        authStreamOut.flush();
        try {
            String in = authStreamIn.readLine().trim();
            while (in.equals(":beat")) {
                in = authStreamIn.readLine().trim();
            }
            int res = Integer.parseInt(in);
            if (res != 0) {
                System.out.println("Error Changing status change!");
            }
        } catch (IOException e) {
            // e.printStackTrace();
        }
    }

    private void serverDetails() {
        JTextField field1 = new JTextField();
        field1.setText(Common.localPORT + "");
        Object[] message = {
                "Local Server Port (5000-65535):", field1,
        };
        int option = JOptionPane.showConfirmDialog(this, message, "Local Server Details", JOptionPane.OK_CANCEL_OPTION);
        if (option == JOptionPane.OK_OPTION) {
            Common.localPORT = Integer.parseInt(field1.getText());
            try {
                running = false;
                if (welcomeSocket != null) {
                    welcomeSocket.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            new Thread(new Runnable() {
                @Override
                public void run() {
                    startServer();
                }
            }).start();
            updatePort();
        }
    }

    private void updatePort() {
        authStreamOut.println("600");
        authStreamOut.flush();
        authStreamOut.println(Common.localPORT);
        authStreamOut.flush();
        try {
            String in = authStreamIn.readLine().trim();
            while (in.equals(":beat")) {
                in = authStreamIn.readLine().trim();
            }
            int res = Integer.parseInt(in);
            if (res != 0) {
                System.out.println("Error port change!");
            }
        } catch (IOException e) {
            // e.printStackTrace();
        }
    }

    private void logout() {
        authStreamOut.println("300");
        authStreamOut.flush();
        try {
            String in = authStreamIn.readLine().trim();
            while (in.equals(":beat")) {
                in = authStreamIn.readLine().trim();
            }

            int res = Integer.parseInt(in);
            if (res == 0) {
                running = false;
                welcomeSocket.close();
                this.dispose();
                Driver.lg = new LoginPane();
                Driver.lg.initialize();

            } else {
                JOptionPane.showMessageDialog(this, "Error Logging Out!", "Error", JOptionPane.ERROR_MESSAGE);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void addFriend() {
        String username = JOptionPane.showInputDialog(this, "Enter username of your friend: ", "Add Friend", JOptionPane.QUESTION_MESSAGE);
        authStreamOut.println("400");
        authStreamOut.flush();
        authStreamOut.println(username);
        authStreamOut.flush();
        try {
            String in = authStreamIn.readLine().trim();
            while (in.equals(":beat")) {
                in = authStreamIn.readLine().trim();
            }
            int res = Integer.parseInt(in);
            if (res == 0) {
                JOptionPane.showMessageDialog(this, username + " successfully added to your friend list!", "Success", JOptionPane.PLAIN_MESSAGE);
            } else if (res == 1) {
                JOptionPane.showMessageDialog(this, "User with username '" + username + "' doesn't exist!", "Error", JOptionPane.ERROR_MESSAGE);
            } else if (res == 2) {
                JOptionPane.showMessageDialog(this, "User already in friend list!", "Error", JOptionPane.PLAIN_MESSAGE);
            } else {
                JOptionPane.showMessageDialog(this, "Some error occured while adding user to your friend list!\nPlease try again!", "Error", JOptionPane.ERROR_MESSAGE);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        getFriendList();
    }

    private void getGroupList() {
        authStreamOut.println("900");
        authStreamOut.flush();
        DefaultListModel<Group> groupDefaultListModel = new DefaultListModel<Group>();
        try {

            groups = new ArrayList<Group>();
            String in = authStreamIn.readLine().trim();
            //System.out.println(in);
            while (!in.equals(":end")) {
                if (!in.trim().equals(":beat")) {
                    String[] temp = in.split(",");
                    String gName = temp[0];
                    String initiator = temp[1];
                    String ip = temp[2];
                    int port = Integer.parseInt(temp[3]);
                    Group g = new Group(gName, initiator, ip, port);
                    groups.add(g);
                    groupDefaultListModel.addElement(g);
                }
                in = authStreamIn.readLine();
                //System.out.println(in);
            }
            groupList.setModel(groupDefaultListModel);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void getFriendList() {
        authStreamOut.println("500");
        authStreamOut.flush();
        DefaultListModel<User> friendList = new DefaultListModel<User>();
        try {
            friends = new ArrayList<User>();
            String in = authStreamIn.readLine().trim();
            //System.out.println(in);
            while (!in.equals(":end")) {
                if (!in.trim().equals(":beat")) {
                    String[] temp = in.split(",");
                    String username = temp[0];
                    int status = Integer.parseInt(temp[1]);
                    String ip = temp[2];
                    int port = Integer.parseInt(temp[3]);
                    User u = new User(username, ip, status, port);
                    friends.add(u);
                    friendList.addElement(u);
                }
                in = authStreamIn.readLine();
                //System.out.println(in);
            }
            list1.setModel(friendList);
            list1.setCellRenderer(new UserRenderer());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void connect(User u) {
        //System.out.println(u.ip+","+u.port+","+u.status);
        if (u.status >= 1) {
            if (activeUsers.contains(u.username)) {
                //System.out.println("Already Active!");
                int i = chatTabs.indexOfTab(u.username);
                chatTabs.setSelectedIndex(i);
            } else {
                try {
                    clientSocket = new Socket(u.ip, u.port);
                    ChatThread ct = new ChatThread(clientSocket, chatTabs, u.username);
                    new Thread(ct).start();
                    activeThreads.add(ct);
                    activeUsers.add(ct.getUsername());
                } catch (IOException e) {
                    e.printStackTrace();
                    JOptionPane.showMessageDialog(this, "Can't connect to " + u.username + "\nPlease refresh friend list and check if user is online or not.", "Unable to Connect", JOptionPane.ERROR_MESSAGE);
                }
            }
        } else {
            JOptionPane.showMessageDialog(this, u.username + " is offline!", "Offline", JOptionPane.ERROR_MESSAGE);
        }
    }

    private void joinGroup(Group g) {
        if (!g.initiator.equals(Common.user)) {
            if (!activeGroups.contains(g.gName)) {
                GroupChat gc = new GroupChat(authStreamIn, authStreamOut, false,g);
                gc.initiate();
                activeGroups.add(g.gName);
            }
        }
    }

    public void setAuthStreamIn(BufferedReader authStreamIn) {
        this.authStreamIn = authStreamIn;
    }

    public void setAuthStreamOut(PrintWriter authStreamOut) {
        this.authStreamOut = authStreamOut;
    }

}


class ChatThread implements Runnable {

    private BufferedReader streamIn;
    private PrintWriter streamOut;
    private Socket clientSocket;
    private volatile StringBuffer toSend;
    private volatile boolean connected = false;

    private JTextField chatLine;
    private JTabbedPane chatTabs;
    private JScrollPane scrollPane;
    private Style style;
    private StyledDocument doc;
    private String username;
    private boolean initiator;
    private boolean dispose = false;
    private boolean started = false;
    private Thread videoChat;
    private ChatArea chatArea;
    private VideoChat vc;

    ChatThread(Socket clientSocket, JTabbedPane chatTabs) {
        this.clientSocket = clientSocket;
        this.chatTabs = chatTabs;
        this.initiator = false;
        init();
    }

    ChatThread(Socket clientSocket, JTabbedPane chatTabs, String username) {
        this.clientSocket = clientSocket;
        this.chatTabs = chatTabs;
        this.username = username;
        this.initiator = true;
        init();
    }

    private void init() {
        //System.out.println("Adding");
        chatArea = new ChatArea();
        scrollPane = chatArea.getScrollPane();
        chatLine = chatArea.getChatLine();
        JButton videoChat = chatArea.getVideoButton();
        JTextPane chatText = chatArea.getChatText();
        JButton sendButton = chatArea.getSendButton();
        toSend = new StringBuffer();
        doc = chatText.getStyledDocument();
        style = doc.addStyle("Style", null);

        try {
            streamIn = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            streamOut = new PrintWriter(clientSocket.getOutputStream());
            connected = true;
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (initiator) {
            streamOut.println(Common.user);
            streamOut.flush();
        } else {
            try {
                username = streamIn.readLine();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        sendButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                send();
            }
        });

        chatLine.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                super.keyPressed(e);
                if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                    send();
                }
            }
        });

        videoChat.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                startVideoChat();
            }
        });

        addTab();

        vc = new VideoChat();
    }

    private void startVideoChat() {
        if (!started) {
            started = true;
            streamOut.println(":video");
            streamOut.flush();
            final String ip = clientSocket.getInetAddress().getHostAddress();
            videoChat = new Thread(new Runnable() {
                @Override
                public void run() {
                    vc.init();
                    vc.startChat(ip);
                }
            }, "Video");
            videoChat.start();
        }
    }

    private void addTab() {
        chatTabs.addTab(username, chatArea.getChatPanel());
        int i = chatTabs.indexOfTab(username);

        JPanel pan = new JPanel();
        JLabel label = new JLabel(username);
        JButton closeButton = new JButton("x");
        closeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int option = JOptionPane.showConfirmDialog(chatArea, "Are you sure you want to close chat with '" + username + "'." +
                        "\nYou will loose all messages!", "End Chat!", JOptionPane.OK_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE);
                if (option == JOptionPane.OK_OPTION) {
                    dispose = true;
                    close();
                }
            }
        });

        label.setBorder(BorderFactory.createEmptyBorder());
        label.setFocusable(false);
        closeButton.setBorder(BorderFactory.createEmptyBorder());
        closeButton.setForeground(Color.RED);
        closeButton.setBackground(new Color(198, 227, 240));

        pan.setBackground(new Color(198, 227, 240));
        pan.setFocusable(false);
        pan.add(label);
        pan.add(closeButton);

        chatTabs.setTabComponentAt(i, pan);
    }

    public String getUsername() {
        return this.username;
    }

    private void send() {
        String in = chatLine.getText();
        if (!in.trim().equals("")) {
            toSend.append(in);
        }
        chatLine.setText("");
    }

    private void close() {
        int i = chatTabs.indexOfTab(username);
        if (i != -1) {
            chatTabs.removeTabAt(i);
        }

        disconnect();
    }

    public void disconnect() {
        connected = false;
        try {
            vc.cleanUp();
            started = false;
        } catch (Exception ignored) {

        }
        ClientPane.activeThreads.remove(this);
        ClientPane.activeUsers.remove(username);
        streamOut.write(":exit");
        streamOut.flush();
        try {
            streamOut.close();
            streamIn.close();
            clientSocket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        JScrollBar vertical = scrollPane.getVerticalScrollBar();
        while (connected) {
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            if (started) {
                try {
                    if (!videoChat.isAlive()) {
                        started = false;
                    }
                } catch (Exception ignored) {

                }
            }

            try {
                byte[] b = new byte[0];
                clientSocket.getOutputStream().write(b);
                clientSocket.getOutputStream().flush();
            } catch (IOException e) {
                //e.printStackTrace();
                if (!dispose) {
                    JOptionPane.showMessageDialog(chatArea, username + " has disconnected the chat!", "Disconnected!", JOptionPane.WARNING_MESSAGE);
                    close();
                    break;
                }
            }

            try {
                if (streamIn.ready()) {
                    // System.out.println("INPUT");
                    String in = streamIn.readLine();
                    if (in.equals(":video")) {
                        startVideoChat();
                        continue;
                    }
                    if (in.equals(":exit")) {
                        if (!dispose) {
                            JOptionPane.showMessageDialog(chatArea, username + " has disconnected the chat!", "Disconnected!", JOptionPane.WARNING_MESSAGE);
                            close();
                            break;
                        }
                    }
                    StyleConstants.setForeground(style, Color.BLUE);
                    doc.insertString(doc.getLength(), username + ": " + in + "\n", style);
                    vertical.setValue(vertical.getMaximum());
                }
            } catch (IOException e) {
                //e.printStackTrace();
            } catch (BadLocationException e) {
                e.printStackTrace();
            }

            if (toSend.length() > 0) {
                String out = toSend.toString();
                StyleConstants.setForeground(style, Color.RED);
                try {
                    doc.insertString(doc.getLength(), "YOU: " + out + "\n", style);
                    vertical.setValue(vertical.getMaximum());
                    streamOut.println(out);
                    streamOut.flush();
                    toSend.setLength(0);
                } catch (BadLocationException e) {
                    e.printStackTrace();
                }
            }

        }
    }

}

class User {

    public String username;
    public String ip;
    public int status;
    public int port;

    public User(String username, String ip, int status, int port) {
        this.username = username;
        this.ip = ip;
        this.status = status;
        this.port = port;
    }

    @Override
    public String toString() {
        return username;
    }

}

class Group {
    public String gName;
    public String ip;
    public int port;
    public String initiator;

    public Group(String gName, String initiator, String ip, int port) {
        this.gName = gName;
        this.ip = ip;
        this.port = port;
        this.initiator = initiator;
    }

    @Override
    public String toString() {
        return gName;
    }
}

class UserRenderer extends JLabel implements ListCellRenderer<User> {

    public UserRenderer() {
        setOpaque(true);
    }

    @Override
    public Component getListCellRendererComponent(JList<? extends User> list, User user, int index,
                                                  boolean isSelected, boolean cellHasFocus) {

        ImageIcon imageIcon = new ImageIcon(Toolkit.getDefaultToolkit().getImage(getClass().getResource("icons/online.png")));
        switch (user.status) {
            case 0:
                imageIcon = new ImageIcon(Toolkit.getDefaultToolkit().getImage(getClass().getResource("icons/offline.png")));
                break;
            case 1:
                imageIcon = new ImageIcon(Toolkit.getDefaultToolkit().getImage(getClass().getResource("icons/online.png")));
                break;
            case 2:
                imageIcon = new ImageIcon(Toolkit.getDefaultToolkit().getImage(getClass().getResource("icons/away.png")));
                break;
            case 3:
                imageIcon = new ImageIcon(Toolkit.getDefaultToolkit().getImage(getClass().getResource("icons/busy.png")));
                break;
            default:
        }

        setIcon(imageIcon);
        setText(user.toString());

        if (isSelected) {
            setBackground(list.getSelectionBackground());
            setForeground(list.getSelectionForeground());
        } else {
            setBackground(list.getBackground());
            setForeground(list.getForeground());
        }

        return this;
    }

}