import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

public class GroupChat {
    public static volatile ArrayList<String> activeUsers;
    public static volatile StringBuffer toSend;
    public JTextArea chatText;
    private JFrame mainFrame;
    private JTextField chatLine;
    private JButton sendButton;
    private JList<String> list1;
    private JPanel panel1;
    private JButton leaveButton;
    private JLabel groupLabel;
    private JButton refreshButton;
    private JScrollPane scrollPane;
    private ServerSocket groupChatSocket;
    private Socket clientSocket;
    private ArrayList<Socket> activeConnections;
    private BufferedReader authStreamIn;
    private PrintWriter authStreamOut;
    private boolean initiator;
    private String gName;
    private Group group;
    private boolean running;

    GroupChat(BufferedReader authStreamIn, PrintWriter authStreamOut, boolean initiator, Group group) {
        this.authStreamIn = authStreamIn;
        this.authStreamOut = authStreamOut;
        this.initiator = initiator;
        this.group = group;
    }

    public synchronized void setToSend(String s) {
        toSend.append(s);
        JScrollBar vertical = scrollPane.getVerticalScrollBar();
        vertical.setValue(vertical.getMaximum());
    }

    public synchronized void broadCast() {
        if (toSend.length() > 0) {
            String s = toSend.toString() + "\n";
            chatText.append(s);
            for (Socket sk : activeConnections) {
                try {
                    sk.getOutputStream().write(s.getBytes());
                    sk.getOutputStream().flush();
                } catch (IOException e) {
                    //e.printStackTrace();
                }
            }
            toSend.setLength(0);
        }
    }

    public void initiate() {
        init();
        if (initiator) {
            createGroup();
        } else {
            joinGroup();
        }
    }

    private void init() {
        leaveButton.setBorder(BorderFactory.createLineBorder(Color.RED, 4));
        leaveButton.setForeground(Color.WHITE);
        toSend = new StringBuffer();

        chatLine.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                super.keyPressed(e);
                if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                    String s = chatLine.getText();
                    if (!s.trim().equals("")) {
                        if (initiator) {
                            setToSend(Common.user + ": " + s);
                            broadCast();
                        } else {
                            setToSend(s);
                        }
                        chatLine.setText("");
                    }
                }
            }
        });

        sendButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String s = chatLine.getText();
                if (!s.trim().equals("")) {
                    if (initiator) {
                        setToSend(Common.user + ": " + s);
                        broadCast();
                    } else {
                        setToSend(s);
                    }
                    chatLine.setText("");
                }
            }
        });

        refreshButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                getList();

            }
        });

        if (initiator) {
            leaveButton.setText("Close Group");
        } else {
            leaveButton.setText("Leave Group");
        }
        JFrame.setDefaultLookAndFeelDecorated(true);
        mainFrame = new JFrame();
        mainFrame.setContentPane(panel1);
        mainFrame.setSize(new Dimension(650, 450));
        mainFrame.setResizable(false);
        removeMinMaxClose(mainFrame);
        mainFrame.setVisible(true);
        activeConnections = new ArrayList<Socket>();
        activeUsers = new ArrayList<String>();
        leaveButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (initiator) {
                    removeGroup();
                } else {
                    leaveGroup();
                }
            }
        });
    }

    private void startServer() {
        activeUsers.add(Common.user);
        while (running) {
            try {
                Socket chatSocket = groupChatSocket.accept();
                if (chatSocket != null) {
                    activeConnections.add(chatSocket);
                    new Thread(new Communication(chatSocket, true, this)).start();
                }
            } catch (IOException e) {
                // e.printStackTrace();
            }
        }
    }

    private void createGroup() {
        int port = 6789;
        while (true) {
            try {
                groupChatSocket = new ServerSocket(port);
                break;
            } catch (IOException e) {
                port++;
                //e.printStackTrace();
            }
        }

        try {
            gName = JOptionPane.showInputDialog(null, "Enter Group Name: ", "Group Name!", JOptionPane.QUESTION_MESSAGE);
            if (gName.trim().length() < 4) {
                JOptionPane.showMessageDialog(null, "Group Name must be atleast 4 character long!\nTry again!", "Error!", JOptionPane.ERROR_MESSAGE);
                cleanUp();
                return;
            }
        } catch (Exception e) {
            cleanUp();
        }

        groupLabel.setText("Group: " + gName);

        authStreamOut.println("888");
        authStreamOut.flush();
        authStreamOut.println(gName);
        authStreamOut.flush();
        authStreamOut.println(port);
        authStreamOut.flush();

        try {
            String in = authStreamIn.readLine().trim();
            while (in.equals(":beat")) {
                in = authStreamIn.readLine().trim();
            }
            int res = Integer.parseInt(in);
            if (res == 0) {
                running = true;
                JOptionPane.showMessageDialog(mainFrame, "Group Started Successfully!", "Success!", JOptionPane.INFORMATION_MESSAGE);
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        startServer();
                    }
                }).start();
            } else if (res == 1) {
                JOptionPane.showMessageDialog(mainFrame, "Group with name '" + gName + "' already exist!\nChoose another name!", "Error!", JOptionPane.ERROR_MESSAGE);
                cleanUp();
            } else if (res == -1) {
                JOptionPane.showMessageDialog(mainFrame, "Some error occured while creating group!\nTry again!", "Error!", JOptionPane.ERROR_MESSAGE);
                cleanUp();
            }
        } catch (IOException e) {
            // e.printStackTrace();
        }
    }

    private void joinGroup() {
        groupLabel.setText("Group: " + group.gName);
        try {
            clientSocket = new Socket(group.ip, group.port);
            new Thread(new Communication(clientSocket, false, this)).start();
        } catch (IOException e) {
            //e.printStackTrace();
            JOptionPane.showMessageDialog(mainFrame, "Unable to join group!", "Error!", JOptionPane.ERROR_MESSAGE);
            cleanUp();
        }
    }

    private void leaveGroup() {
        try {
            clientSocket.getOutputStream().write(":exit".getBytes());
        } catch (Exception e) {
            //e.printStackTrace();
        } finally {
            cleanUp();
        }
    }

    private void removeGroup() {
        authStreamOut.println("999");
        authStreamOut.flush();
        authStreamOut.println(gName);
        authStreamOut.flush();

        try {
            String in = authStreamIn.readLine().trim();
            while (in.equals(":beat")) {
                in = authStreamIn.readLine().trim();
            }
            int res = Integer.parseInt(in);
            if (res == 0) {
                cleanUp();
            } else {
                JOptionPane.showMessageDialog(mainFrame, "Error Removing Group!", "Error", JOptionPane.ERROR_MESSAGE);
            }
        } catch (Exception e) {
            //e.printStackTrace();
        }
    }

    private void getList() {
        if (!initiator) {
            try {
                setToSend(":list");
                BufferedReader streamIn = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
                String in = streamIn.readLine();
                // System.out.println(in);
                while (!in.contains(":list")) {
                    chatText.append(in);
                    in = streamIn.readLine();
                }
                String[] users = in.split(",");
                DefaultListModel<String> defaultListModel = new DefaultListModel<String>();
                for (int i = 1; i < users.length; i++) {
                    defaultListModel.addElement(users[i]);
                }
                list1.setModel(defaultListModel);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            DefaultListModel<String> defaultListModel = new DefaultListModel<String>();
            for (String s : activeUsers) {
                defaultListModel.addElement(s);
            }
            list1.setModel(defaultListModel);
        }
    }

    private void cleanUp() {
        running = false;
        ClientPane.activeGroups.remove(gName);
        mainFrame.dispose();
        JFrame.setDefaultLookAndFeelDecorated(false);

        try {
            groupChatSocket.close();
        } catch (Exception e) {
            //e.printStackTrace();
        }
        for (Socket s : activeConnections) {
            try {
                s.close();
            } catch (Exception e) {
                // e.printStackTrace();
            }
        }
        try {
            clientSocket.close();
        } catch (Exception e) {
            // e.printStackTrace();
        }
    }

    private void removeMinMaxClose(Component comp) {

        if (comp instanceof JMenu) {
            comp.getParent().remove(comp);
        }
        if (comp instanceof JButton) {
            String accName = comp.getAccessibleContext().getAccessibleName();
            if (accName.equals("Maximize") || accName.equals("Iconify") ||
                    accName.equals("Close")) comp.getParent().remove(comp);
        }
        if (comp instanceof Container) {
            Component[] comps = ((Container) comp).getComponents();
            for (Component comp1 : comps) {
                removeMinMaxClose(comp1);
            }
        }
    }
}

class Communication implements Runnable {

    private Socket chatSocket;
    private BufferedReader streamIn;
    private PrintWriter streamOut;
    private boolean initiator;
    private GroupChat gc;

    Communication(Socket chatSocket, boolean initiator, GroupChat gc) {
        this.gc = gc;
        this.chatSocket = chatSocket;
        this.initiator = initiator;
        try {
            streamIn = new BufferedReader(new InputStreamReader(chatSocket.getInputStream()));
            streamOut = new PrintWriter(chatSocket.getOutputStream());
        } catch (IOException e) {
            //e.printStackTrace();
        }
    }

    private void server() {
        try {
            String username = streamIn.readLine();
            GroupChat.activeUsers.add(username);
            gc.setToSend(username + " joined group!");
            gc.broadCast();

            while (!chatSocket.isClosed()) {
                if (streamIn.ready()) {
                    String s = streamIn.readLine();
                    // System.out.println(s);
                    if (s.trim().contains(":list")) {
                        String temp = ":list,";
                        for (String user : GroupChat.activeUsers) {
                            temp = temp + "," + user;
                        }
                        streamOut.println(temp);
                        streamOut.flush();
                        continue;
                    }
                    if (s.trim().contains(":exit")) {
                        break;
                    }
                    gc.setToSend(username + ": " + s);
                    gc.broadCast();
                }
            }
            gc.setToSend(username + " left group!");
            gc.broadCast();
            GroupChat.activeUsers.remove(username);
            streamIn.close();
            streamOut.close();
            chatSocket.close();
        } catch (Exception e) {
            // e.printStackTrace();
        }
    }

    private void client() {
        streamOut.println(Common.user);
        streamOut.flush();
        while (!chatSocket.isClosed()) {
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (GroupChat.toSend.length() > 0) {
                String s = GroupChat.toSend.toString();
                streamOut.println(s);
                streamOut.flush();
                GroupChat.toSend.setLength(0);
            }
            try {
                if (streamIn.ready()) {
                    String s = streamIn.readLine();
                    if (!s.trim().equals("")) {
                        gc.chatText.append(s + "\n");
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void run() {
        if (initiator) {
            server();
        } else {
            client();
        }
    }
}
