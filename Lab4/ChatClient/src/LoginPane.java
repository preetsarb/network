import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetSocketAddress;
import java.net.Socket;

public class LoginPane extends JFrame {
    private JTabbedPane tabbedPane1;
    private JPanel panel1;
    private JButton signUpButton;
    private JTextField signupName;
    private JButton loginButton;
    private JTextField loginName;
    private JPasswordField loginPassword;
    private JPasswordField signupPassword;
    private JButton authenticationServerButton;
    private JButton exitButton;
    private JPasswordField reSignUpPassword;

    private Socket socket;
    private BufferedReader streamIn;
    private PrintWriter streamOut;
    private boolean connected = false;

    public void initialize() {
        initGUI();
    }

    private void connectToServer() {
        try {
            socket = new Socket();
            socket.connect(new InetSocketAddress(Common.authServerIP, Common.authServerPORT), 1000);
            connected = true;
        } catch (IOException e) {
            JOptionPane.showMessageDialog(this, "Unable to connect to Authentication Server.\nPlease check the Authentication Server Settings.", "Error!", JOptionPane.ERROR_MESSAGE);
            serverDetails();
            //e.printStackTrace();
        }

        if (connected) {
            try {
                streamIn = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                streamOut = new PrintWriter(socket.getOutputStream());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void cleanUP() {
        connected = false;
        if (socket != null && !socket.isClosed()) {
            try {
                socket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
       // System.out.println("Closed!");
    }

    private void initGUI() {

        Font font = new Font("Cambria", Font.BOLD, 14);


        loginButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                login();
            }
        });

        loginButton.setBackground(new Color(109, 132, 180));
        loginButton.setForeground(Color.WHITE);
        loginButton.setFocusPainted(false);
        loginButton.setFont(font);

        signUpButton.setBackground(new Color(109, 132, 180));
        signUpButton.setForeground(Color.WHITE);
        signUpButton.setFocusPainted(false);
        signUpButton.setFont(font);

        signUpButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                signUp();
            }
        });

        authenticationServerButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                serverDetails();
            }
        });

        authenticationServerButton.setFocusPainted(false);

        exitButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cleanUP();
                System.exit(0);
            }
        });

        this.setContentPane(panel1);
        this.setResizable(false);
        this.setSize(new Dimension(500, 250));
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.setVisible(true);
    }

    private void login() {
        connectToServer();
        if (connected) {
            String username = loginName.getText();
            String password = new String(loginPassword.getPassword());
            streamOut.println("200");
            streamOut.flush();
            streamOut.println(username);
            streamOut.flush();
            streamOut.println(password);
            streamOut.flush();
            streamOut.println("7777");
            streamOut.flush();

            try {

                String in = streamIn.readLine().trim();
                while (in.equals(":beat")) {
                    in = streamIn.readLine().trim();
                }

                int res = Integer.parseInt(in);
                if (res == 0) {
                    Common.user = username;
                    Driver.cl = new ClientPane();
                    Driver.cl.setAuthStreamIn(streamIn);
                    Driver.cl.setAuthStreamOut(streamOut);
                    this.dispose();
                    Driver.cl.initialize();
                } else if (res == 1) {
                    JOptionPane.showMessageDialog(this, "Wrong Username or Password!", "Error!", JOptionPane.ERROR_MESSAGE);
                } else {
                    JOptionPane.showMessageDialog(this, "An unknown error has occurred, please try again!!", "Error!", JOptionPane.ERROR_MESSAGE);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void signUp() {
        connectToServer();
        if (connected) {
            String username = signupName.getText().trim();
            String password = new String(signupPassword.getPassword()).trim();
            String rePassword = new String(reSignUpPassword.getPassword()).trim();
            signupPassword.setText("");
            reSignUpPassword.setText("");
            if(!password.equals(rePassword)){
                JOptionPane.showMessageDialog(this, "Your password doesn't match!\nPlease enter again!", "Error!", JOptionPane.ERROR_MESSAGE);
                return;
            }
            if (username.length() < 4 || password.length() < 4) {
                JOptionPane.showMessageDialog(this, "Username and Password should be of at least 4 characters!", "Error!", JOptionPane.ERROR_MESSAGE);
            } else {
                streamOut.println("100");
                streamOut.flush();
                streamOut.println(username);
                streamOut.flush();
                streamOut.println(password);
                streamOut.flush();

                try {
                    String in = streamIn.readLine().trim();
                    while (in.equals(":beat")) {
                        in = streamIn.readLine().trim();
                    }

                    int res = Integer.parseInt(in);
                    if (res == 0) {
                        JOptionPane.showMessageDialog(this, "Registration Successful!\nYou can login now!", "Success!", JOptionPane.INFORMATION_MESSAGE);
                        loginName.setText(username);
                        tabbedPane1.setSelectedIndex(0);
                    } else if (res == 1) {
                        JOptionPane.showMessageDialog(this, "User already exists!", "Error!", JOptionPane.ERROR_MESSAGE);
                    } else {
                        JOptionPane.showMessageDialog(this, "An unknown error has occurred, please try again!!", "Error!", JOptionPane.ERROR_MESSAGE);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void serverDetails() {
        JTextField field1 = new JTextField();
        JTextField field2 = new JTextField();
        field1.setText(Common.authServerIP);
        field2.setText(Common.authServerPORT + "");
        Object[] message = {
                "Server IP:", field1,
                "Server Port:", field2,
        };
        int option = JOptionPane.showConfirmDialog(null, message, "Authentication Server Details", JOptionPane.OK_CANCEL_OPTION);
        if (option == JOptionPane.OK_OPTION) {
            Common.authServerIP = field1.getText();
            Common.authServerPORT = Integer.parseInt(field2.getText());
        }
    }

}
