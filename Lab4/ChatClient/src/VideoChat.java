import org.bytedeco.javacpp.opencv_core.IplImage;
import org.bytedeco.javacv.CanvasFrame;
import org.bytedeco.javacv.FrameGrabber;
import org.bytedeco.javacv.OpenCVFrameGrabber;

import javax.imageio.ImageIO;
import javax.sound.sampled.*;
import javax.swing.*;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.*;


public class VideoChat {
    private DatagramSocket videoSocket;
    private DatagramSocket audioSocket;
    private InetAddress ipAddress;
    private OpenCVFrameGrabber grabber;
    private CanvasFrame canvasFrame;
    private int Video_PORT = 50000;
    private int Audio_PORT = 50001;
    private TargetDataLine targetDataLine;
    private SourceDataLine sourceDataLine;
    private boolean initialized = true;

    public void init() {
        initializeComponents();
    }

    private void initializeComponents() {
        try {
            videoSocket = new DatagramSocket(Video_PORT);
            audioSocket = new DatagramSocket(Audio_PORT);
            videoSocket.setSoTimeout(2000);
            audioSocket.setSoTimeout(2000);
        } catch (SocketException e) {
            //e.printStackTrace();
            initialized = false;
        }

        grabber = new OpenCVFrameGrabber(0);
        grabber.setImageWidth(24);
        grabber.setImageHeight(32);
        grabber.setFrameRate(10);
        grabber.setFormat("jpg");
        grabber.setAudioChannels(0);


        if (AudioSystem.isLineSupported(Port.Info.MICROPHONE)) {
            try {
                DataLine.Info inputDataLineInfo = new DataLine.Info(TargetDataLine.class, getAudioFormat());
                targetDataLine = (TargetDataLine) AudioSystem.getLine(inputDataLineInfo);
                DataLine.Info outputDataLineInfo = new DataLine.Info(SourceDataLine.class, getAudioFormat());
                sourceDataLine = (SourceDataLine) AudioSystem.getLine(outputDataLineInfo);
            } catch (Exception e) {
                // e.printStackTrace();
                initialized = false;
            }
        } else {
            initialized = false;
        }

    }

    public void startChat(String ip) {
        try {
            grabber.start();
            targetDataLine.open(getAudioFormat());
            targetDataLine.start();
            sourceDataLine.open(getAudioFormat());
            sourceDataLine.start();
            ipAddress = InetAddress.getByName(ip);
            canvasFrame = new CanvasFrame("Video Chat");
            canvasFrame.setCanvasSize(320, 240);
            canvasFrame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        } catch (Exception e) {
            // e.printStackTrace();
            initialized = false;
        }

        if (initialized) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    audio();
                }
            }).start();

            new Thread(new Runnable() {
                @Override
                public void run() {
                    video();
                }
            }).start();
        } else {
            JOptionPane.showMessageDialog(null, "Can't start video chat due to some error!", "Error", JOptionPane.ERROR_MESSAGE);
            cleanUp();
        }

    }

    private void video() {
        try {
            IplImage grabbedImage;
            while (canvasFrame.isVisible() && (grabbedImage = grabber.grab()) != null) {
                byte[] imageInByte;
                BufferedImage bufferedImage = grabbedImage.getBufferedImage();
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                //video
                try {
                    ImageIO.write(bufferedImage, "jpg", byteArrayOutputStream);
                    byteArrayOutputStream.flush();
                    imageInByte = byteArrayOutputStream.toByteArray();
                    byteArrayOutputStream.close();
                    DatagramPacket sendPacket = new DatagramPacket(imageInByte, imageInByte.length, ipAddress, Video_PORT);
                    videoSocket.send(sendPacket);

                    try {
                        byte[] receiveVideo = new byte[57 * 1024];
                        DatagramPacket receivePacket = new DatagramPacket(receiveVideo, receiveVideo.length);
                        videoSocket.receive(receivePacket);
                        InputStream in = new ByteArrayInputStream(receivePacket.getData());
                        BufferedImage bImageFromConvert = ImageIO.read(in);
                        canvasFrame.showImage(bImageFromConvert);
                    } catch (SocketTimeoutException e) {
                        // e.printStackTrace();
                    }

                } catch (IOException e) {
                    // e.printStackTrace();
                }

                //Thread.yield();
            }
        } catch (FrameGrabber.Exception e) {
            //e.printStackTrace();
        }
        cleanUp();
    }

    private void audio() {
        while (canvasFrame.isVisible() && targetDataLine.isOpen()) {
            try {
                byte[] audioBuffer = new byte[8000];
                targetDataLine.read(audioBuffer, 0, audioBuffer.length);
                audioSocket.send(new DatagramPacket(audioBuffer, audioBuffer.length, ipAddress, Audio_PORT));
                try {
                    byte[] receiveAudio = new byte[8000];
                    DatagramPacket datagram = new DatagramPacket(receiveAudio, receiveAudio.length);
                    audioSocket.receive(datagram);
                    sourceDataLine.write(datagram.getData(), 0, datagram.getData().length);
                    sourceDataLine.drain();
                } catch (SocketTimeoutException e) {
                    // e.printStackTrace();
                }
            } catch (IOException e) {
                // e.printStackTrace();
            }

        }

        cleanUp();
    }

    public void cleanUp() {

        try {
            grabber.stop();
        } catch (FrameGrabber.Exception e) {
            e.printStackTrace();
        }
        try {
            audioSocket.close();
        } catch (Exception ignored) {

        }
        try {
            videoSocket.close();
        } catch (Exception ignored) {

        }
        try {
            targetDataLine.close();
        } catch (Exception ignored) {

        }
        try {
            sourceDataLine.close();
        } catch (Exception ignored) {

        }
        if (canvasFrame != null) {
            canvasFrame.dispose();
        }
    }

    public AudioFormat getAudioFormat() {
        float sampleRate = 8000.0F;
        int sampleSizeInBits = 8;
        int channels = 1;
        return new AudioFormat(sampleRate, sampleSizeInBits, channels, true, false);
    }

}
