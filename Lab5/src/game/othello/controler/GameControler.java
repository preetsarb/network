package game.othello.controler;

import game.othello.model.Game;
import java.util.HashMap;

public class GameControler {

    private static HashMap<String, Game> games;
    private static HashMap<String, Game> savedGames;
    public final static GameControler gameControler = new GameControler();

    public GameControler() {
        games = new HashMap<>();
        savedGames = new HashMap<>();
    }

    public void addGame(String ssid, Game g) {
        games.put(ssid, g);
    }

    public Game getGame(String ssid) {
        return games.get(ssid);
    }

    public void saveGame(String ssid) {
        Game g = games.remove(ssid);
        g.removePlayers();
        savedGames.put(ssid, g);
    }

    public Game resumeGame(String ssid) {
        if (savedGames.containsKey(ssid)) {
            Game g = savedGames.remove(ssid);
            games.put(ssid, g);
            return g;
        } else {
            return null;
        }
    }

    public String getList() {
        String list = "";
        for (String s : games.keySet()) {
            list += s + ",";
        }
        return list;
    }

}
