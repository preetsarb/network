package game.othello.controler;

import game.othello.model.Game;
import java.io.IOException;
import javax.json.JsonObject;
import javax.json.spi.JsonProvider;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class GameServer extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        RequestDispatcher view1 = request.getRequestDispatcher("game.jsp");
        if (view1 != null) {
            view1.forward(request, response);
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        if (request.getSession().getAttribute("user") == null) {
            response.sendRedirect("Welcome");
        } else {
            //System.out.println(request.getServletPath());
            processRequest(request, response);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        if (request.getSession().getAttribute("user") == null) {
            response.sendRedirect("Welcome");
        } else {
            String action = request.getParameter("action");
            switch (action) {
                case "create":
                    String ssid = request.getSession().getId();
                    Game g = new Game(ssid);
                    g.addPlayer();
                    GameControler.gameControler.addGame(ssid, g);
                    request.getSession().setAttribute("player", 1);
                    request.getSession().setAttribute("game", g);
                    processRequest(request, response);
                    break;
                case "getgames":
                    response.getWriter().println(GameControler.gameControler.getList());
                    break;
                case "join":
                    ssid = request.getParameter("ssid");
                    g = GameControler.gameControler.getGame(ssid);
                    if ((g != null) && !(g.getPlayers() > 1)) {
                        g.addPlayer();
                        request.getSession().setAttribute("player", 2);
                        request.getSession().setAttribute("game", g);
                        response.getWriter().print("true");
                    } else {
                        response.getWriter().print("false");
                    }
                    break;
                case "save":
                    ssid = request.getParameter("ssid");
                    g = GameControler.gameControler.getGame(ssid);
                    JsonProvider provider = JsonProvider.provider();
                    JsonObject move = provider.createObjectBuilder()
                            .add("action", "save")
                            .build();
                    g.sendMove(move, (int) request.getSession().getAttribute("player"));
                    GameControler.gameControler.saveGame(ssid);
                    response.getWriter().print(ssid);
                    break;
                case "resume":
                    try {
                        ssid = request.getParameter("ssid");
                        g = GameControler.gameControler.resumeGame(ssid);
                        if (g != null) {
                            g.addPlayer();
                            request.getSession().setAttribute("player", 1);
                            request.getSession().setAttribute("game", g);
                            processRequest(request, response);
                        } else {
                            response.sendRedirect("Welcome");
                        }
                    } catch (ServletException | IOException e) {
                        response.sendRedirect("Welcome");
                    }
                    break;
                case "leave":
                    try {
                        request.getSession().removeAttribute("game");
                        response.getWriter().print("true");
                    } catch (Exception e) {
                        response.getWriter().print("false");
                    }
                    break;
                default:
            }
        }
    }

}
