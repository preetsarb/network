package game.othello.controler;

import game.othello.model.Game;
import java.io.IOException;
import java.io.StringReader;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ApplicationScoped;
import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;

@ApplicationScoped
@ServerEndpoint(value = "/game/{ssid}/{player}")
public class GameWebSocket {

    @OnOpen
    public void open(Session session, @PathParam("ssid") String ssid, @PathParam("player") int player) throws IOException {
        if (player == 1) {
            GameControler.gameControler.getGame(ssid).setSession1(session);
        }
        if (player == 2) {
            GameControler.gameControler.getGame(ssid).setSession2(session);
        }
    }

    @OnClose
    public void close(Session session) {

    }

    @OnError
    public void onError(Throwable error) {
        Logger.getLogger(GameWebSocket.class.getName()).log(Level.SEVERE, null, error);
    }

    @OnMessage
    public void handleMessage(String message, Session session, @PathParam("ssid") String ssid, @PathParam("player") int player) {
        Game g = GameControler.gameControler.getGame(ssid);
        if (g.getTurn() == player) {
            try (JsonReader reader = Json.createReader(new StringReader(message))) {
                JsonObject jsonMessage = reader.readObject();
                if (jsonMessage.getString("action").equals("play")) {
                    g.setStarted();
                    String board = jsonMessage.getString("box");
                    g.setBoard(board);
                    g.toggleturn();
                    g.sendMove(jsonMessage, player);
                }
                if (jsonMessage.getString("action").equals("timeout")) {
                     g.toggleturn();
                     g.sendTimeout(jsonMessage);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

}
