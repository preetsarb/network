package game.othello.model;

import javax.json.JsonObject;
import javax.websocket.EncodeException;
import javax.websocket.Session;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Game {

    private Session s1;
    private Session s2;
    private final String ssid;
    private int players;
    private int turn;
    private final char[][] board;
    private boolean started;

    public Game(String ssid) {
        this.ssid = ssid;
        this.players = 0;
        started = false;
        board = new char[8][8];
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                if (i > 2 && j > 2 && i < 5 && j < 5) {
                    board[j][i] = (((i + j) % 2) == 0) ? 'w' : 'b';
                } else {
                    board[j][i] = '0';
                }
            }
        }
        turn = 1;
    }

    public void setSession1(Session s) {
        s1 = s;
    }

    public void setSession2(Session s) {
        s2 = s;
    }

    public void setStarted(){
        this.started = true;
    }
    
    public boolean getState(){
        return started;
    }
    
    public String getssid() {
        return ssid;
    }

    public void addPlayer() {
        players++;
    }
    
    public void removePlayers(){
        players = 0;
    }

    public int getPlayers() {
        return players;
    }

    public int getTurn() {
        return turn;
    }

    public void toggleturn() {
        if (turn == 1) {
            turn = 2;
        } else {
            turn = 1;
        }
    }

    public char[][] getBoard(){
        return board;
    }
    
    public String getBoardAsString() {
        String send = "";
        for(int i = 0;i < 8;i++){
            for(int j = 0;j < 8;j++){
                send += board[i][j]+",";
            }
        }
        return send;
    }

    public void setBoard(String boardString) {
        String[] in = boardString.split(",");
        int k = 0, t = 0;
        for (String d : in) {
            board[k][t] = d.charAt(0);
            k++;
            if (k == 8) {
                k = 0;
                t++;
            }
        }
    }

    public void sendMove(JsonObject move, int player) {
        try {
            if (player == 1) {
                s2.getBasicRemote().sendObject(move);
            }
            if (player == 2) {
                s1.getBasicRemote().sendObject(move);
            }
        } catch (IOException | EncodeException ex) {
            Logger.getLogger(Game.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void sendTimeout(JsonObject timeout){
        try {
            s1.getBasicRemote().sendObject(timeout);
            s2.getBasicRemote().sendObject(timeout);
        } catch (IOException | EncodeException ex) {
            Logger.getLogger(Game.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
