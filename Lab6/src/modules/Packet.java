package modules;

/**
 * Created by Sarbpreet on 4/6/2015.
 */
public class Packet implements Comparable<Packet> {
    public int id;
    public int sourceID;
    public double timestamp;

    public Packet(int id, int sId, double ts) {
        this.id = id;
        sourceID = sId;
        timestamp = ts;
    }

    @Override
    public int compareTo(Packet p) {
        if (this.timestamp > p.timestamp) {
            return 1;
        } else if (this.timestamp < p.timestamp) {
            return -1;
        } else {
            return 0;
        }
    }
}