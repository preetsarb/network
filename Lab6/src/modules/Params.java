package modules;

/**
 * Created by Sarbpreet on 4/12/2015.
 */

public class Params {
    public static double packetSize = 10;        //10Kb
    public static int numberSRC = 1;
    public static int switchQueueSize = 10;     //number of packets
    public static double srcBW = 100;             // Kbps
    public static int srcPGR = 50;            //number of packets per second
    public static double switchBW = 200;         // Kbps
}
