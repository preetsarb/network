package modules;

import java.util.PriorityQueue;
import java.util.Queue;

/**
 * Created by Sarbpreet on 4/12/2015.
 */
public class Sink {
    public int[] packetRequired;
    public Queue<Packet>[] receiveQueue;

    public Sink(int nSources) {
        packetRequired = new int[Params.numberSRC];
        for (int i = 0; i < Params.numberSRC; i++) {
            packetRequired[i] = 0;
        }
        receiveQueue = new Queue[nSources];
        for (int i = 0; i < nSources; i++) {
            receiveQueue[i] = new PriorityQueue<Packet>();
        }
    }

}
