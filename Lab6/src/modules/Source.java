package modules;

import java.util.HashMap;
import java.util.PriorityQueue;
import java.util.Queue;

/**
 * Created by Sarbpreet on 4/6/2015.
 */

public class Source {
    public int id;
    public int pgr;
    public double linkBW;
    public int cwnd;
    public int ackRecieved;
    public int ducpAcks;
    public int packetCount;
    public Queue<Packet> srcQueue;
    public HashMap<Integer, Packet> sentPackets;
    public int ssthreshold;
    public int packetDropped;


    public Source(int id, int pgr, double linkBW) {
        this.id = id;
        this.pgr = pgr;
        this.linkBW = linkBW;
        srcQueue = new PriorityQueue<>();
        sentPackets = new HashMap<>();
        cwnd = 1;
        packetCount = 0;
        ackRecieved = -1;
        ssthreshold = Integer.MAX_VALUE;
        ducpAcks = 0;
        packetDropped = 0;
    }

}
