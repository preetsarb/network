package modules;

import java.util.PriorityQueue;
import java.util.Queue;

/**
 * Created by Sarbpreet on 4/6/2015.
 */
public class Switch {
    public double linkBW;
    public Queue<Packet> buffer;
    public Queue<Packet> ackQueue;
    public int packetCount;
    public boolean flag;

    public Switch(double linkBW) {
        this.linkBW = linkBW;
        packetCount = 0;
        flag = true;
        buffer = new PriorityQueue<>();
        ackQueue = new PriorityQueue<>();
    }
}
