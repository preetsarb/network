package reno;

import modules.*;

/**
 * Created by Sarbpreet on 4/6/2015.
 */

public class Event implements Comparable<Event> {

    TYPE type;
    double time;
    Source src;
    Switch sw;
    Sink sink;
    int id;

    public Event(TYPE type, double time, Source src, Switch sw) {
        this.type = type;
        this.time = time;
        this.src = src;
        this.sw = sw;
        this.sink = Simulation.sink;
    }

    public Event(TYPE type, double time, Source src, Switch sw, int id) {
        this.type = type;
        this.time = time;
        this.src = src;
        this.sw = sw;
        this.sink = Simulation.sink;
        this.id = id;
    }

    public void performAction() {
        switch (type) {
            case PACKET_GENERATION:
                generatePacket();
                break;
            case TRANSMISSION_TO_SWITCH:
                transmitPacket();
                break;
            case TRANSMISSION_TO_SINK:
                receivePacket();
                break;
            case ACK_TO_SWITCH:
                sendAck();
                break;
            case ACK_TO_SOURCE:
                receiveAck();
                break;
            case TIMEOUT:
                timeout();
                break;
            default:
        }
    }

    public void generatePacket() {                           //packet generation at source

        //System.out.println(src.cwnd);
        src.sentPackets.clear();
        for (int i = 0; i < src.cwnd; i++) {
            Packet p = new Packet(src.packetCount, src.id, time + 1 / src.pgr);
            src.srcQueue.add(p);
            Event e = new Event(TYPE.TRANSMISSION_TO_SWITCH, time, src, sw);
            Simulation.ev.add(e);
            double timeout = time + 4 * (Params.packetSize / src.linkBW + Params.packetSize / sw.linkBW);  //RTT
            e = new Event(TYPE.TIMEOUT, timeout, src, sw, src.packetCount);
            Simulation.ev.add(e);
            src.sentPackets.put(src.packetCount, p);
            //System.out.println("Sent:" + src.packetCount);
            src.packetCount++;
            time += 0.0000000001;
        }
    }

    public void transmitPacket() {                      //transmission of packet from source to switch

        double nextTime = (time + (Params.packetSize / src.linkBW));
        if (src.srcQueue.size() > 0) {
            Packet p = src.srcQueue.poll();
            if (sw.packetCount < Params.switchQueueSize) {
                sw.buffer.add(p);
                sw.packetCount++;
            } else {
                src.packetDropped += 1;
            }
        }

        if (sw.flag) {
            Event e = new Event(TYPE.TRANSMISSION_TO_SINK, nextTime, src, sw);
            Simulation.ev.add(e);
            sw.flag = false;
        }

    }

    public void receivePacket() {                            //packet from switch to sink
        if (sw.packetCount > 0) {
            double nextTime = (time + ((Params.packetSize) / sw.linkBW));
            Packet p = sw.buffer.poll();
            // System.out.println(p.id);
            sink.receiveQueue[src.id - 1].add(p);
            sw.packetCount--;
            Event e = new Event(TYPE.TRANSMISSION_TO_SINK, nextTime, src, sw);
            Simulation.ev.add(e);
            nextTime = time + 0.0000001;
            e = new Event(TYPE.ACK_TO_SWITCH, nextTime, src, sw);
            Simulation.ev.add(e);
        } else {
            sw.flag = true;
            double nextTime = (time + ((Params.packetSize) / sw.linkBW));
            Event e = new Event(TYPE.TRANSMISSION_TO_SINK, nextTime, src, sw);
            Simulation.ev.add(e);
        }
    }

    public void sendAck() {                                    //send ack of received packet from sink to switch

        Packet p = sink.receiveQueue[src.id - 1].poll();
        //  System.out.println(p.id);
        if (p.id == sink.packetRequired[src.id - 1]) {
            sink.packetRequired[src.id - 1]++;
            sw.ackQueue.add(p);
        } else {
            p.id = sink.packetRequired[src.id - 1];
            sw.ackQueue.add(p);
        }

        double nextTime = time + Params.packetSize / sw.linkBW;
        Event e = new Event(TYPE.ACK_TO_SOURCE, nextTime, src, sw);
        Simulation.ev.add(e);

    }

    public void receiveAck() {                               // send ack from switch to source
        Packet p = sw.ackQueue.poll();
        //System.out.println("Ack: "+p.id);
        // System.out.println(p.id+" "+ src.ackRecieved);
        if (p.id > src.ackRecieved) {
            // System.out.println("Ack: "+p.id);
            src.ackRecieved = p.id;
            src.sentPackets.remove(p.id);
            if (src.cwnd < src.ssthreshold) {
                src.cwnd++;
            } else if (src.sentPackets.isEmpty()) {
                src.cwnd++;
            }

            if (src.sentPackets.isEmpty()) {
                Event e = new Event(TYPE.PACKET_GENERATION, time + 0.00000001, src, sw);
                Simulation.ev.add(e);
            }

        } else {
            src.ducpAcks++;
            if (src.ducpAcks == 3) {
                //System.out.println("Ack: " + p.id);
                src.cwnd = src.cwnd / 2;
                sink.packetRequired[src.id - 1] = src.packetCount;
                Event e = new Event(TYPE.PACKET_GENERATION, time + 0.00000001, src, sw);
                Simulation.ev.add(e);
                src.ducpAcks = 0;
            }
        }
    }

    public void timeout() {                                   //timeout
        if (src.sentPackets.containsKey(id)) {
            //System.out.println("Cwnd:"+src.);
            src.ssthreshold = src.cwnd / 2;
            //System.out.println(src.ssthreshold);
            src.cwnd = 1;
            sink.packetRequired[src.id - 1] = src.packetCount;
            Event e = new Event(TYPE.PACKET_GENERATION, time + 0.00000001, src, sw);
            Simulation.ev.add(e);
        }
    }


    @Override
    public int compareTo(Event e) {
        if (this.time > e.time) {
            return 1;
        } else if (this.time < e.time) {
            return -1;
        } else {
            return 0;
        }
    }

    public enum TYPE {
        PACKET_GENERATION, TRANSMISSION_TO_SWITCH, TRANSMISSION_TO_SINK, ACK_TO_SWITCH, ACK_TO_SOURCE, TIMEOUT
    }
}
