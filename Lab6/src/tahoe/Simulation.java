package tahoe;

import modules.*;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Random;

/**
 * Created by Sarbpreet on 4/13/2015.
 */
public class Simulation {

    public static Queue<Event> ev;
    public static Sink sink;

    public static void main(String[] args) throws IOException {
        int nSources = Params.numberSRC;
        Source[] src = new Source[nSources];

        for (int i = 0; i < nSources; i++) {
            src[i] = new Source(i + 1, Params.srcPGR, Params.srcBW);
        }

        Switch sw = new Switch(Params.switchBW);
        sink = new Sink(Params.numberSRC);

        ev = new PriorityQueue<>();                    //event queue
        Random r = new Random();

        for (int i = 0; i < nSources; i++) {
            Event tmp = new Event(Event.TYPE.PACKET_GENERATION, (r.nextDouble()), src[i], sw);
            ev.add(tmp);
        }

        BufferedWriter[] bw = new BufferedWriter[nSources];          //files for individual source data

        for (int i = 0; i < nSources; i++) {
            bw[i] = new BufferedWriter(new FileWriter("src" + (i + 1) + ".txt"));
            bw[i].write("Src" + (i + 1) + "\t\tThroughput\t\tTime");   //modules.Source queue size  +  switch queue size  +  packets dropped  +  time  +  type of event
            bw[i].newLine();
        }

        int N =5000;
        while (N > 0) {

            Event e = ev.poll();
            int sid = e.src.id - 1;
            if ( e.type == Event.TYPE.PACKET_GENERATION) {
                double thr = 0;
                if(e.src.packetCount > 0) {
                    thr = ((double) e.src.packetCount - (double) e.src.packetDropped) / (double) e.src.packetCount;
                }
                bw[sid].write(e.src.cwnd + "\t\t" + thr + "\t\t" + e.time);
                bw[sid].newLine();
            }
            e.performAction();
            N--;
        }
        for (int i = 0; i < nSources; i++) {
            bw[i].close();
        }

        System.out.println("Done!");

    }
}
